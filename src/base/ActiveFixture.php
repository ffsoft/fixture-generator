<?php

namespace ffsoft\fixtureGenerator\base;

use ffsoft\fixtureGenerator\controllers\GeneratorController;
use Yii;

class ActiveFixture extends \yii\test\ActiveFixture
{
    /**
     * Loads the fixture.
     *
     * It populate the table with the data returned by [[getData()]].
     *
     * If you override this method, you should consider calling the parent implementation
     * so that the data returned by [[getData()]] can be populated into the table.
     */
    public function load()
    {
        $this->data = [];
        $table = $this->getTableSchema();
        $generator = $this->getData();

        $buffer = [];
        foreach ($generator as $alias => $row) {
            $buffer[] = $row;
            if (count($buffer) > 1000) {
                $this->db->createCommand()->batchInsert($table->fullName, array_keys($buffer[0]), $buffer)->execute();
                $buffer = [];
            }
        }
        if (!empty($buffer)) {
            $this->db->createCommand()->batchInsert($table->fullName, array_keys($buffer[0]), $buffer)->execute();
        }
    }

    protected function getData()
    {
        if (null !== $this->dataFile) {
            if (($handle = fopen(Yii::getAlias($this->dataFile), 'r')) !== false) {
                $schema = $this->readRow($handle);
                while (($data = $this->readRow($handle)) !== false) {
                    $data = array_combine($schema, $data);
                    yield $data;
                }
                fclose($handle);
            }

            return;
        }

        return parent::getData();
    }

    protected function readRow($filePointer)
    {
        $data = fgetcsv($filePointer);
        if (false !== $data) {
            foreach ($data as $key => &$value) {
                $value = GeneratorController::NULL_PLACEHOLDER === $value ? null : $value;
            }
        }

        return $data;
    }
}