<?php

namespace ffsoft\fixtureGenerator\helpers;

use Exception;
use yii\helpers\FileHelper;

class FileWriter
{
    /** @var bool|resource  */
    protected $fp;

    public function __construct(string $fileName)
    {
        FileHelper::createDirectory(dirname($fileName), $mode = 0775, $recursive = true);
        $this->fp = fopen($fileName, 'w');
        if (false === $this->fp) {
            throw  new Exception('Cannot write file ' . $fileName );
        }
    }

    /**
     * @param string $line
     *
     * @return $this
     */
    public function write(string  $line): self
    {
        fwrite($this->fp, $line . PHP_EOL);
        return $this;
    }

    /**
     * @return $this
     */
    public function close(): self
    {
        fclose($this->fp);
        return $this;
    }
    
}