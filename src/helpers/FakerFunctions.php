<?php

namespace ffsoft\fixtureGenerator\helpers;

use common\helpers\IBAN;
use common\models\Accounts;
use Faker\Factory;
use Faker\Generator;
use yii\base\InvalidConfigException;

class FakerFunctions
{
    /**
     * Settings for changing functions
     *
     * @var array
     */
    protected $config;
    /**
     * @var Generator
     */
    protected $faker;

    public function __construct(array $config)
    {
        $this->config = $config;
        $this->faker = Factory::create();
    }

    public function accountNumber($record, $field, $value)
    {
        static $currentNumber;
        if (empty($currentNumber)) {
            $currentNumber = 1234567899;
        }
        if (isset($this->config['accountNumberCallable']) && is_callable($this->config['accountNumberCallable'])) {
            $currentNumber = $this->config['accountNumberCallable']($currentNumber);
        } elseif (is_callable('\common\models\Accounts::generateNumberInternal')) {
            $currentNumber = Accounts::generateNumberInternal($currentNumber);
        } else {
            throw new InvalidConfigException('$config[accountNumberCallable] is not callable');
        }

        return $currentNumber;
    }

    public function accountIban($record, $field, $value)
    {
        return IBAN::generate($record[$field]);
    }
    public function address($record, $field, $value)
    {
        return $this->faker->address;
    }

    public function authKey($record, $field, $value)
    {
        return $this->faker->md5;
    }

    public function authTfKey($record, $field, $value)
    {
        return $this->faker->asciify(str_repeat('*', 16));
    }

    public function city($record, $field, $value)
    {
        return $this->faker->city;
    }

    public function companyName($record, $field, $value)
    {
        return $this->faker->company;
    }

    public function country($record, $field, $value)
    {
        return $this->faker->countryISOAlpha3;
    }

    public function email($record, $field, $value)
    {
        return $this->faker->email;
    }

    public function firstName($record, $field, $value)
    {
        return $this->faker->firstName;
    }

    public function lastName($record, $field, $value)
    {
        return $this->faker->lastName;
    }

    public function nationalNumber($record, $field, $value)
    {
        return $this->faker->ean13;
    }

    public function password($record, $field, $value)
    {
        return password_hash($this->config['defaultPassword'] ?? 'support', PASSWORD_DEFAULT);
    }

    public function phoneNumber($record, $field, $value)
    {
        return $this->faker->phoneNumber;
    }

    public function postcode($record, $field, $value)
    {
        return $this->faker->postcode;
    }

    public function setEmpty($record, $field, $value)
    {
        return '';
    }

    public function setNull($record, $field, $value)
    {
        return null;
    }

    public function setZero($record, $field, $value)
    {
        return 0;
    }

    public function userName($record, $field, $value)
    {
        return in_array($value, $this->config['protectedUserNames'] ?? []) ? $value : $this->faker->userName;
    }

    public function __call($name, $arguments)
    {
        return $this->faker->{$name};
    }
}