<?php

namespace ffsoft\fixtureGenerator;

use Yii;

class FixtureGeneratorModule extends \yii\base\Module
{
    /**
     * @var string
     */
    public $controllerNamespace = 'ffsoft\fixtureGenerator\controllers';
    /**
     * Settings passed to Faker functions
     *
     * @var array
     */
    public $fakerSettings
        = [
            'defaultPassword'    => 'support',
            'protectedUserNames' => ['root'],
        ];
    /**
     * Path to fixtures
     *
     * @var string
     */
    public $fixturePath = '@common/fixtures';
    /**
     * Path to fixtures data files. Could be full path or alias
     *
     * @var string
     */
    public $fixtureDataPath = '@common/fixtures/data';
    /**
     * Namespace for fixtures
     *
     * @var string
     */
    public $fixtureNamespace = 'common\fixtures';
    /**
     * Run chown -R 1000:1000 over $fixturesPath or not
     *
     * @var bool
     */
    public $performChown = false;
    /**
     * [имя таблицы][имя поля] = метод для замены
     *
     * @var array[][]
     */
    public $rules = [];
    /**
     * Обработать только таблицы подходящие хотя бы под один из шаблонов.
     * Пустой массив - разрешено все
     *
     * @var array
     */
    public $only = [];
    /**
     * Не обрабатывать таблицу если она подходит хотя бы под один из шаблонов даже если указано в $only
     * Пустой массив - разрешено все
     *
     * @var array
     */
    public $exclude = [];
    /**
     * Имя файла для скрипта загрузки фикстур, если пусто то файл создан не будет
     *
     * @var string
     */
    public $script = '@root/bin/fixtures.sh';
    /**
     * Имя файла консольных команд Yii
     *
     * @var string
     */
    public $yiiConsole = 'yii';
    /**
     * Имя таблицы с миграциями
     *
     * @var string
     */
    public $migrationsTable = 'migration';

    public function init()
    {
        parent::init();

        $this->script = Yii::getAlias($this->script);
        $this->yiiConsole = Yii::getAlias($this->yiiConsole);
        $this->fixturePath = Yii::getAlias($this->fixturePath);
    }
}