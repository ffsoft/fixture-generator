<?php

namespace ffsoft\fixtureGenerator\controllers;

use ffsoft\fixtureGenerator\FixtureGeneratorModule;
use ffsoft\fixtureGenerator\helpers\FakerFunctions;
use ffsoft\fixtureGenerator\helpers\FileWriter;
use Yii;
use yii\base\InvalidArgumentException;
use yii\base\InvalidConfigException;
use yii\base\NotSupportedException;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\db\Connection;
use yii\db\Exception;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\BaseInflector;
use yii\helpers\Console;
use yii\helpers\FileHelper;

/**
 * Generate fixtures in CSV
 *
 * @package ffsoft\fixtureGenerator\controllers
 */
class GeneratorController extends Controller
{
    /** CSV "из коробки" не позволяет сохранять null, поэтому вместо null будет записана этот случайный UUID v4 */
    const NULL_PLACEHOLDER = 'a3ec140e-4276-44e7-9086-53fb923647a5';
    /** @var string Regexp for tables excluded from process, has priority over $only */
    public $exclude;
    /** @var string Regexp for tables only included to process */
    public $only;
    /** @var string Filename for loading script, otherwise file would not be created */
    public $script;
    /** @var string Migrations table name */
    public $migrationsTable;
    /** @var FakerFunctions */
    protected $fakerFunctions;
    /** @var Connection */
    protected $db;
    /**
     * Обработать только таблицы подходящие хотя бы под один из шаблонов.
     * Пустой массив - разрешено все
     *
     * @var array
     */
    protected $onlyArray = [];
    /**
     * Не обрабатывать таблицу если она подходит хотя бы под один из шаблонов даже если указано в $only
     * Пустой массив - разрешено все
     *
     * @var array
     */
    protected $excludeArray = [];
    /**
     * @var FixtureGeneratorModule the module that this controller belongs to.
     */
    public $module;

    /**
     * Create fixtures
     *
     * @throws InvalidConfigException
     * @throws NotSupportedException
     * @throws Exception
     */
    public function actionGenerate()
    {
        $this->prepareOptions();
        $this->stdout('Removing previous fixtures...' . PHP_EOL);
        $this->removeFixtureFiles($this->module->fixturePath);
        $this->removeDataFiles($this->module->fixturePath);
        $this->stdout('Previous fixteres removed' . PHP_EOL, Console::FG_GREEN);
        $tablesNames = $this->getTableNames();
        foreach ($tablesNames as $tableName) {
            if (!$this->canProcess($tableName)) {
                $this->stdout('Table ' . $tableName . ' is skipped' . PHP_EOL, Console::FG_YELLOW);
                continue;
            }
            $this->stdout('Process table ' . $tableName . '...' . PHP_EOL);
            $csv = $this->createFiles($tableName);
            $limit = 1000;
            $offset = 0;
            while ($records = (new Query())->from($tableName)->limit($limit)->offset($offset)->createCommand($this->db)
                ->queryAll()) {
                $records = $this->makeItFake($records, $tableName);
                foreach ($records as $record) {
                    $this->saveRow($csv, $record);
                }
                $offset = $offset + $limit;
            }
            $this->stdout('Fixture for table ' . $tableName . ' is created' . PHP_EOL, Console::FG_GREEN);
        }
        $this->generateLoadingScript();
        $this->chown();

        return ExitCode::OK;
    }

    /**
     * Truncates database
     *
     * @throws Exception
     */
    public function actionTruncate()
    {
        if (!YII_ENV_DEV) {
            return ExitCode::UNSPECIFIED_ERROR;
        }

        $dbName = $this->db->createCommand('SELECT DATABASE()')->queryScalar();
        $this->stdout('Truncating database `' . $dbName . '`...' . PHP_EOL);
        $createSql = $this->db->createCommand('SHOW CREATE DATABASE `' . $dbName . '`')->queryOne();
        $this->db->createCommand('DROP DATABASE `' . $dbName . '`')->execute();
        $this->db->createCommand($createSql['Create Database'])->execute();
        $this->stdout('Database `' . $dbName . '` is truncated' . PHP_EOL);

        return ExitCode::OK;
    }

    /**
     * Init function
     */
    public function init()
    {
        parent::init();
        $this->db = Yii::$app->getDb();
        $this->fakerFunctions = new FakerFunctions($this->module->fakerSettings);
    }

    /**
     * @return array
     */
    public function optionAliases()
    {
        return ['o' => 'only', 'e' => 'exclude', 's' => 'script'];
    }

    /**
     * @param string $actionID
     *
     * @return array|string[]
     */
    public function options($actionID)
    {
        return ['only', 'exclude', 'script'];
    }

    /**
     * @param $tableName
     *
     * @return bool
     */
    protected function canProcess($tableName): bool
    {
        foreach ($this->excludeArray as $regex) {
            if (preg_match($regex, $tableName)) {
                return false;
            }
        }
        foreach ($this->onlyArray as $regex) {
            if (preg_match($regex, $tableName)) {
                return true;
            }
        }

        return empty($this->onlyArray); // Если нет ограничения Only - разрешены все
    }

    /**
     * Chown
     */
    protected function chown()
    {
        if ($this->module->performChown) {
            exec('chown -R 1000:1000 ' . $this->module->fixturePath);
            $this->stdout('Chown on ' . $this->module->fixturePath . ' is done' . PHP_EOL, Console::FG_GREEN);
            if ($this->script) {
                exec('chown 1000:1000 ' . $this->script);
                $this->stdout('Chown on ' . $this->script . ' is done' . PHP_EOL, Console::FG_GREEN);
            }
        }
    }

    /**
     * Generate content for fixture class
     *
     * @param array $values
     *
     * @return string
     */
    protected function classTemplate(array $values)
    {
        $template = <<<'ENDTEMPLATE'
<?php
namespace %namespace%;

use ffsoft\fixtureGenerator\base\ActiveFixture;

class %className% extends ActiveFixture
{
    public $dataFile = '%dataFilePath%';
    public $tableName = '%tableName%';
}
ENDTEMPLATE;

        $template = strtr($template, $values);

        return $template;
    }

    /**
     * Create files for fixtures
     * Return file pointer to resource CSV
     *
     * @param $tableName
     *
     * @return resource
     */
    protected function createFiles($tableName)
    {
        $className = BaseInflector::camelize($tableName) . 'Fixture';
        $dataFileAlias = $this->module->fixtureDataPath . DIRECTORY_SEPARATOR . $tableName . '.csv';
        $fixtureFileAlias = $this->module->fixturePath . DIRECTORY_SEPARATOR . $className . '.php';
        
        $dataFilePath = Yii::getAlias($dataFileAlias);
        $fixtureFilePath = Yii::getAlias($fixtureFileAlias);

        FileHelper::createDirectory(dirname($dataFilePath), $mode = 0775, $recursive = true);

        file_put_contents(
            $fixtureFilePath,
            $this->classTemplate(
                [
                    '%namespace%'    => $this->module->fixtureNamespace,
                    '%className%'    => $className,
                    '%dataFilePath%' => $dataFileAlias,
                    '%tableName%'    => $tableName,
                ]
            )
        );

        $dataFile = fopen($dataFilePath, 'w');
        $columnNames = $this->db->getTableSchema($tableName)->columnNames;
        fputcsv($dataFile, $columnNames);

        return $dataFile;
    }

    /**
     * Стандартное получение списка таблиц Yii::$app->db->getSchema()->getTableNames(); не работает, потому что
     * возвращает и VIEW. Отличить VIEW от таблицы для MySQL нельзя, т.к. ViewFinderTrait определен только для MSSQL и
     * PgSql Пришлось написать этот костыль
     *
     * @return array
     * @throws Exception
     */
    protected function getTableNames()
    {
        $dbName = $this->db->createCommand("SELECT DATABASE()")->queryScalar();
        $list = $this->db->createCommand('SHOW FULL TABLES IN `' . $dbName . '` WHERE TABLE_TYPE LIKE "BASE TABLE";')
            ->queryAll();
        $tableNames = [];
        foreach ($list as $value) {
            $tableNames[] = reset($value);
        }

        return $tableNames;
    }

    /**
     * @param array  $records
     * @param string $tableName
     *
     * @return array
     */
    protected function makeItFake(array $records, string $tableName)
    {
        if (in_array($tableName, array_keys($this->module->rules))) {
            foreach ($records as $key => $record) {
                foreach ($record as $field => $value) {
                    $config = ArrayHelper::getValue($this->module->rules, $tableName . '.' . $field, null);
                    $fakeRecord = $record;
                    $fakeField = $field;
                    $fakeValue = $value;
                    
                    if (is_string($config)) {
                        $method = $config;
                    } elseif (is_array($config) && isset($config['method'])) {
                        $method = $config['method'];
                        $fakeRecord = $config['record'] ?? $fakeRecord;
                        $fakeField = $config['field'] ?? $fakeField;
                        $fakeValue = $config['value'] ?? $fakeValue;
                    } else {
                        continue;
                    }
                    $records[$key][$field] = $this->fakerFunctions->{$method}($fakeRecord, $fakeField, $fakeValue);
                }
            }
        }

        return $records;
    }

    /**
     * Prepare Options
     */
    protected function prepareOptions()
    {
        $only = $this->module->only;
        if (null !== $this->only) {
            $only = $this->only;
        }
        $exclude = $this->module->exclude;
        if (null !== $this->exclude) {
            $exclude = $this->exclude;
        }
        $this->onlyArray = $this->prepareRegexp($only);
        $this->excludeArray = $this->prepareRegexp($exclude);

        if (empty($this->script) && !empty($this->module->script)) {
            $this->script = $this->module->script;
        }

        if (!empty($this->script)) {
            $this->migrationsTable = $this->module->migrationsTable;
        }
    }

    /**
     * @param string|array|bool|null $value
     *
     * @return array
     */
    protected function prepareRegexp($value)
    {
        if (true === $value) {
            return [];
        }
        if (null === $value) {
            return [];
        }
        if (is_string($value)) {
            $value = explode(',', $value);
        }
        if (!is_array($value)) {
            throw new InvalidArgumentException();
        }
        foreach ($value as &$val) {
            $val = trim($val);
            $val = str_replace('*', '[A-z0-9_-]*', $val);
            $val = str_replace('?', '[A-z0-9_-]{1}', $val);
            $val = '/^' . $val . '$/';
        }

        return $value;
    }

    /**
     * @param $directory
     */
    protected function removeDataFiles($directory)
    {
        foreach (glob("{$directory}/data/*.csv") as $file) {
            unlink($file);
        }
    }

    /**
     * @param $directory
     */
    protected function removeFixtureFiles($directory)
    {
        foreach (glob("{$directory}/*Fixture.php") as $file) {
            unlink($file);
        }
    }

    /**
     * @param resource $filePointer
     * @param array    $data
     */
    protected function saveRow($filePointer, array $data)
    {
        foreach ($data as $key => &$value) {
            $value = null === $value ? static::NULL_PLACEHOLDER : $value;
        }

        fputcsv($filePointer, $data, ',', '"');
    }

    /**
     * @throws Exception
     */
    protected function generateLoadingScript()
    {
        if (empty($this->script)) {
            $this->stdout('Fixture loading script wont be created' . PHP_EOL, Console::FG_RED);

            return;
        }

        $scriptFile = new FileWriter($this->script);
        $lastVersion = $this->db
            ->createCommand('SELECT * FROM `' . $this->migrationsTable . '` ORDER BY `apply_time` DESC')
            ->queryScalar();

        $scriptFile
            ->write('#!/bin/sh')
            ->write('php ' . $this->module->yiiConsole . ' ' . $this->module->id . '/generator/truncate \\')
            ->write('&& php ' . $this->module->yiiConsole . ' migrate/to "' . $lastVersion . '" --interactive=0 \\')
            ->write('&& php ' . $this->module->yiiConsole . ' fixture/load "*" --interactive=0 \\')
            ->write('&& php ' . $this->module->yiiConsole . ' migrate/up --interactive=0')
            ->close();

        $this->stdout('Fixture loading ' . $this->script . ' script was created' . PHP_EOL, Console::FG_GREEN);
    }
}